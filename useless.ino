#include <Servo.h>
#include <NewPing.h>

Servo myservo;

//Position of servo
int pos = 0;       
//Change speed of servo         
long timeDelay;

#define echoPin 11
#define trigPin 12
#define buttonPin 2
#define led1 5

//Distance from box
int distance = 0; 
//Used for position of servo and brightness of led
int num = 0;
//
long count = random(1, 3);

NewPing sonar(trigPin, echoPin, 200); //For sonar sonar.ping_cm() sensor

void setup()
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  pinMode(buttonPin, INPUT); //switch
  pinMode(led1, OUTPUT); //1 pin for 2 leds
  Serial.begin (9600);
}

void loop()
{
  
  //Figure out distance from box, if out of range set it to 200
  Serial.print("Ping: ");
  if(sonar.ping() == 0){ // If out of range
    distance = 200;
    Serial.print(200);
  }
  else{
    distance = sonar.ping_cm(); // Convert ping time to cm
    Serial.print(distance); 
  }
  Serial.println("cm");
  //If distance in range and is different
  if(distance <= 50 && distance >= 30){
    //Decide on brightness and arm distance depending on distance
    num = (50 - sonar.ping_cm());
    //Arm sticks out and led brightens depending on distamce
    if(pos < num){
      for(pos = myservo.read(); pos <= num; pos++){
        analogWrite(led1, pos);
        myservo.write(pos);
        delay(15);
      }
      analogWrite(led1, num);
    }
    //Arm goes back in and led dim depending on distamce
    else if(pos > num){
      for(pos = myservo.read(); pos >= num; pos--){
        myservo.write(pos);
        analogWrite(led1, pos);
        delay(15);
      }
      analogWrite(led1, num);
    }
  }
  
  //Out of range so arm goes back in and led turns off
  else{
    analogWrite(led1, 0);
    for(pos = myservo.read(); pos <= 90; pos++){
      myservo.write(pos);
      delay(15);
    }
  }
  
  //Switch
  if(!digitalRead(buttonPin)){
    count--;
    if(count <= 0){
      for(int i = 0; i < 3; i++){
        timeDelay = random(1, 4);
        for(pos = 100; pos >= 60; pos -= timeDelay){
          myservo.write(pos);
          
          delay(15);
        }
        //Go back
        for(pos = myservo.read(); pos <= 100; pos++){
          myservo.write(pos);
          delay(15);
        }
      }
      count = random(1, 4);
    }
    //Go push switch
    timeDelay = random(1, 4);
    for(pos = 100; pos >= 0; pos -= timeDelay){
      myservo.write(pos);
      delay(15);
    }
    //Go back
    for(pos = myservo.read(); pos <= 100; pos++){
      myservo.write(pos);
      delay(15);
    }
  }
  delay(15);
}